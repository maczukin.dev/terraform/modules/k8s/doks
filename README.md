# DigitalOcean Kubernetes service terraform module

Terraform module to configure DigitalOcean Kubernetes service.

## Author

Tomasz Maczukin, 2021

## License

MIT
