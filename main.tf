provider "digitalocean" {
  token = var.digitalocean_token
}

data "digitalocean_kubernetes_versions" "default" {}

resource "digitalocean_kubernetes_cluster" "default" {
  name    = var.cluster_name
  region  = var.digitalocean_region
  version = data.digitalocean_kubernetes_versions.default.latest_version

  node_pool {
    name = "${var.cluster_name}-pool"
    size = var.digitalocean_node_size

    auto_scale = true
    min_nodes  = 1
    max_nodes  = 3
  }
}
