terraform {
  required_version = "~> 0.14"

  required_providers {
    digitalocean = {
      version = "~> 2.4"
      source  = "digitalocean/digitalocean"
    }
  }
}
