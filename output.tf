output "kubeconfig" {
  description = "kubectl configuration"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].raw_config
}

output "kubecontext" {
  description = "kubectl context"
  value       = "do-${var.digitalocean_region}-${var.cluster_name}"
}

output "k8s_host" {
  description = "Cluster's host"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].host
}

output "k8s_ca_certificate" {
  description = "Cluster's CA certificate"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].cluster_ca_certificate
}

output "k8s_client_certificate" {
  description = "Cluster's client certificate"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].client_certificate
}

output "k8s_client_key" {
  description = "Cluster's client key"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].client_key
}

output "k8s_token" {
  description = "Cluster's access token"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].token
}

output "k8s_credentials_expires_at" {
  description = "Date and time when cluster's credentials will expire"
  value       = digitalocean_kubernetes_cluster.default.kube_config[0].expires_at
}
