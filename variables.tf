variable "digitalocean_token" {
  description = "Token used to authenticate in DigitalOcean"
  type        = string
}

variable "digitalocean_region" {
  description = "DigitalOcean region where the resources will be deployed"
  type        = string
  default     = "ams3"
}

variable "digitalocean_node_size" {
  description = "Size of DigitalOcean droplets that will be used to power the Kubernetes cluster"
  type        = string
  default     = "s-1vcpu-2gb"
}

variable "cluster_name" {
  description = "Name for the created Kubernetes cluster"
  type        = string
}

variable "kubeconfig" {
  description = "Path to Kubernetes connection configuration file"
  type        = string
}
